package render;

import model.Main;
import objects.pendulum.Pendulum1;
import objects.pendulum.Pendulum2;
import processing.core.PVector;

public abstract class ForceThunk implements RenderThunk{

	PendRender target;
	
	int r,g,b;
	
	@Override
	public void render(Main m) {
		m.pushStyle();
		PVector renderPos = renderPos();
		m.strokeWeight(2);
		m.stroke(r,g,b);
		//x,y=0 is center of screen
		m.line(m.width/2+renderPos.x*m.scale,m.height/2-renderPos.y*m.scale,
				m.width/2+getFulcrum().x*m.scale,m.height/2-getFulcrum().y*m.scale);
		m.popStyle();
		
	}
	public ForceThunk(PendRender t, int r, int g, int b) {
		target=t;
		this.r=r;
		this.g=g;
		this.b=b;
	}
	
	public PVector getFulcrum() {
		return target.renderPos();
	}
	
	public abstract PVector renderPos();
	
}
