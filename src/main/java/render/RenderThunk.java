package render;

import model.Main;

public interface RenderThunk {

	public void render(Main m);
	
}
