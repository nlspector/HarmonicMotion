package render;

import model.Main;

public class ScaleRender implements RenderThunk{

	@Override
	public void render(Main m) {
		m.pushMatrix();
		m.pushStyle();
		m.stroke(255);
		m.fill(255);
		m.strokeWeight(5);
		m.line(m.width-2*m.scale, m.height-0.25f*m.scale,m.width-1*m.scale,m.height-0.25f*m.scale);
		m.popMatrix();
		
		m.textSize(24);
		m.text("1m",m.width-1.5f*m.scale,m.height-0.4f*m.scale);
		m.popStyle();

		
	}

}
