package render;

import model.Main;
import processing.core.PVector;

public class StringThunk implements RenderThunk{

	PVector fulc;
	PendRender target;
	
	@Override
	public void render(Main m) {
		m.pushStyle();
		PVector renderPos = renderPos();
		m.strokeWeight(2);
		m.stroke(255);
		//x,y=0 is center of screen
		m.line(m.width/2+renderPos.x*m.scale,m.height/2-renderPos.y*m.scale,
				m.width/2+getFulcrum().x*m.scale,m.height/2-getFulcrum().y*m.scale);
		m.popStyle();
		
	}
	public StringThunk(PVector f, PendRender ro) {
		fulc=f;
		target=ro;
	}
	
	public PVector getFulcrum() {
		return fulc;
	}
	
	public PVector renderPos() {
		return PVector.add(getFulcrum(), PVector.mult(PVector.fromAngle(target.getTheta()-3.14f/2f),target.getL()));
	}
	
	
}
