package render;

import model.Main;
import objects.TranslationalObject;
import processing.core.PVector;

public class SpringRender implements RenderThunk{
	
	PVector e1;
	TranslationalObject obj,o2;
	
	int r=255;
	int g = 0;
	int b = 0;
	
	public SpringRender(PVector e1, TranslationalObject obj) {
		this.e1=e1; this.obj=obj;
	}
	
	public SpringRender(TranslationalObject o2, TranslationalObject obj) {
		this.o2=o2; this.obj=obj;
	}

	
	
	@Override
	public void render(Main m) {
		if(o2==null) {
			render(m, e1, obj.getPos());
		} else {
			render(m, o2.getPos(), obj.getPos());

		}
		
		
	}
	
	public void setRGB(int r, int g, int b) {
		this.r=r;
		this.g=g;
		this.b=b;
	}
	
	public void render(Main m, PVector pos1, PVector pos2) {
		m.pushMatrix();
		m.pushStyle();
		m.translate(m.width/2,m.height/2);
		m.stroke(r,g,b);
		m.strokeWeight(getThickness(pos1,pos2)*m.scale/200f);
		m.line(pos1.x*m.scale, -pos1.y*m.scale,pos2.x*m.scale,-pos2.y*m.scale);
		m.popStyle();
		m.popMatrix();
	}
	
	public float getThickness(PVector pos1, PVector pos2) {
		return 10/(PVector.sub(pos1,pos2).mag()+0.5f);
	}

}
