package render;

import processing.core.PVector;

public interface PendRender {

	public float getTheta();
	
	public float getL();
	
	public PVector renderPos();
	
	
	
}
