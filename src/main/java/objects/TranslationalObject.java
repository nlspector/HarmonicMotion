package objects;

import java.util.ArrayList;
import java.util.List;

import forces.Force;
import graph.DataSet;
import model.Listener;
import model.Main;
import model.Observation;
import processing.core.PVector;
import render.RenderThunk;

public abstract class TranslationalObject implements RenderThunk, PEObject{

	private List<Force> forces;
	protected PVector r;
	private PVector v;
	protected PVector a;
	protected float m;
	
	float v_dir=1;
	
	float t;
	
	DataSet ry_dataset=new DataSet(255,0,0,"time","y-position");
	DataSet rx_dataset=new DataSet(255,0,0,"time","x-position");

	DataSet ax_dataset=new DataSet(255,0,0,"time","x-position");

	protected boolean rk = false;

	
	public PVector getPos() {
		return r;
	}
	
	public TranslationalObject(PVector ir, PVector iv, PVector ia, float m) {
		r=ir;
		setVel(iv);
		a=ia;
		this.m=m;
		forces = new ArrayList<>();
		
	}
	
	public void setRk(boolean b) {
		rk=b;
	}
	
	
	public void addForce(Force f) {
		forces.add(f);
	}
	
	public float getMass() {
		return m;
	}
	
	public void update(float dt) {
		float dta=0.0166667f;
		if(a!=null && a.mag() !=0)dta=0.01f/a.mag();
		if(rk)updateRungeKutta(dt);
		else updateEuler(dt);
		//System.out.println(rk + " " + dta + " " + a.mag());
	}
	
	public void updateEuler(float dt) {
		PVector sumForces= new PVector();
		for(int i = 0; i < forces.size(); i++) {
			sumForces.add(forces.get(i).getForce(dt));

		}
		
		t+=dt;
		a = PVector.div(sumForces, m);
		
		v.add(PVector.mult(a, dt));
		r.add(PVector.mult(getVel(), dt));

		//if(v.magSq() < 0.01) Main.printTime();
		if(Math.signum(v.x)!= v_dir) { Main.printTime2(); System.out.println(" " + r.x);}
		v_dir=Math.signum(v.x);
		
		
		ry_dataset.addPoint(t, r.y);
		rx_dataset.addPoint(t, r.x);
		ax_dataset.addPoint(t, a.x);
	}
	
	/* Runge-Kutta Method
	 * 
	 * In Euler's Method, an approximation of y given dy/dt is found 
	 * by y_n+1 = y_n + dy/dt * h
	 * 
	 * This is a first-order approximation of y. Expand y as a Taylor series:
	 * 
	 * y = y0*t^0 + dy/dt * t^1 + ... [O(n^2)]
	 * 
	 * So the error is proportional to x^2
	 * 
	 * Runge-Kutta aims to alleviate that problem by taking slopes at different times.
	 * The form, ultimately, is the same:
	 * 
	 * y = y0 + mh
	 * 
	 * but m is just a much better approximation, a weighted average of 
	 * certain slopes over the interval.
	 * 
	 * In our case--we first must solve for v, given a
	 * 
	 * 
	 * */
	

	public PVector getA(float dt, PVector r, PVector v) {
		PVector sumForces= new PVector();
		for(int i = 0; i < forces.size(); i++) {
			sumForces.add(forces.get(i).getForce(dt,r,v));

		}
		return PVector.div(sumForces, m);
		
	}
	
	//returns: a very good approximation of what the velocity will be
	//at the END of the interval
	public PVector rkVel(float dt, PVector rloc, PVector vloc) {
		
		PVector k1 = getA(dt,rloc,vloc);
		//k2 = the slope at the midpoint of the interval,
		//using k1 to get an approximation.
		PVector vel2 = PVector.add(vloc, PVector.mult(k1, dt/2f));
		PVector k2 = getA(dt,rloc,vel2);

		//k3 = the slope at the midpoint of the interval,
		//this time using k2 to get an approximation.
		PVector vel3 = PVector.add(vloc, PVector.mult(k2, dt/2f));
		PVector k3 = getA(dt,rloc,vel3);

		//finally, k4 = the slope at the endpoint of the interval,
		//using k3 to get an approximation.
		PVector vel4 = PVector.add(vloc, PVector.mult(k3, dt));
		PVector k4 = getA(dt,rloc,vel4);
		//IMPORTANT--this method does NOT give you a good representation of what "a" should be
		a=PVector.mult(PVector.add(PVector.add(k1,k4),(PVector.mult(PVector.add(k2, k3),2))), 
				1/6f);
		return PVector.add(v,PVector.mult(a,dt));
	}
	
	public void rkPos(float dt) {
		//k1 = v--current velocity
		//r2 = estimate of position 
		PVector r2 = PVector.add(r, PVector.mult(v, dt/2f));
		PVector k2 =rkVel(dt/2f,r2,v);
		PVector r3 = PVector.add(r, PVector.mult(k2, dt/2f));
		PVector k3 =rkVel(dt/2f,r3,k2);
		PVector r4 = PVector.add(r, PVector.mult(k3, dt));
		PVector k4 = rkVel(dt,r4,k3);
		r.add(PVector.mult(PVector.add(PVector.add(v,k4),(PVector.mult(PVector.add(k2, k3),2))), 
				1/(6f*dt)));
	}
	
	public void updateRungeKutta(float dt) {
		t+=dt;
		//update v first
		//k1 = the first approximation of the slope
		//which is the slope at the beginning of the interval
		v=rkVel(dt,r,v);
		
		//k1 = estimate of the slope (i.e., velocity) at beginning of interval
		//PVector k1 =rkVel(dt,r,v);
		
//		PVector r2 = PVector.add(r, PVector.mult(k1, dt/2f));
//		PVector k2 =rkVel(dt,r2,k1);
//		PVector r3 = PVector.add(r, PVector.mult(k2, dt/2f));
//		PVector k3 =rkVel(dt,r3,k2);
//		PVector r4 = PVector.add(r, PVector.mult(k3, dt));
//		PVector k4 = rkVel(dt,r4,k3);
//		v=PVector.mult(PVector.add(PVector.add(k1,k4),(PVector.mult(PVector.add(k2, k3),2))), 
//				1/6f);

		//v=PVector.add(k1,k2).mult(0.5f);
		//let's see if I did it right
		r.add(PVector.mult(v, dt));
		//rkPos(dt);
		
		ry_dataset.addPoint(t, r.y);
		rx_dataset.addPoint(t, r.x);
		ax_dataset.addPoint(t, a.x);

	}
	
	
	
	
	public PVector getVel() {
		return v;
	}

	public void setVel(PVector v) {
		this.v=v;
	}
	
	List<Listener> listeners = new ArrayList<>();
	
	public void addListener(Listener l) {
		listeners.add(l);
	}
	
	public void removeListener(Listener l) {
		listeners.remove(l);
	}
	
	public void observation(Observation o) {
		for(Listener l: listeners) {
			l.observed(o);
		}
	}
	
	public DataSet getYPositionDataSet(int r, int g, int b) {
		return ry_dataset.withColor(r,g,b);
	}
	
	public DataSet getXPositionDataSet(int r, int g, int b) {
		return rx_dataset.withColor(r,g,b);
	}
	
	public DataSet getXAccelDataSet(int r, int g, int b) {
		return ax_dataset.withColor(r,g,b);
	}
	
	public void setPos(PVector r) {
		this.r=r;
	}
	
	
}
