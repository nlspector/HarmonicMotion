package objects;

import model.Main;
import processing.core.PVector;
import render.RenderThunk;

public class DiscPendulumThunk implements RenderThunk{
	
	PendulumObject o;
	
	public DiscPendulumThunk(PendulumObject o) {
		this.o=o;
	}
	
	@Override
	public void render(Main m) {
		PVector renderPos = PVector.add(o.fulcrum, PVector.mult(PVector.fromAngle(o.getTheta()-3.14f/2f),o.l));
		m.pushMatrix();
		m.pushStyle();
		m.translate(m.width/2+renderPos.x*m.scale,m.height/2-renderPos.y*m.scale);
		m.fill(255,0,0);
		//x,y, dimz, dimy--since o.l=RADIUS of disc, mult by 2
		m.ellipse(0,0,2*o.getL()*m.scale,2*o.getL()*m.scale);
		m.popStyle();

		m.popMatrix();
		
		
	}
}
