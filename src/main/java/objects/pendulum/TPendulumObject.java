package objects.pendulum;

import forces.Gravity;
import forces.Tension;
import graph.DataSet;
import model.Main;
import objects.TranslationalObject;
import processing.core.PVector;

public class TPendulumObject extends TranslationalObject{

	float L;
	PVector fulcrum;
	DataSet L_dataset=new DataSet(255,0,0,"time","L");
	float t;
	
	public TPendulumObject(PVector ir, float m, PVector fulcrum) {
		super(ir, new PVector(0,0), new PVector(0,0), m);
		this.L=PVector.sub(ir,fulcrum).mag();
		this.fulcrum=fulcrum;
		Gravity g = new Gravity(this);
		addForce(g);
		addForce(new Tension(this, fulcrum,g,L));
		// TODO Auto-generated constructor stub
	}

	@Override
	public void update(float dt) {
		super.update(dt);
		t+=dt;
		
		L_dataset.addPoint(t, PVector.sub(this.getPos(), fulcrum).mag()- L);
	}
	
	@Override
	public void render(Main pe) {
		pe.pushMatrix();
		if(rk) {
			pe.fill(0,255,0);
		} else {
			pe.fill(255,0,0);

		}
		pe.translate(pe.width/2+r.x*pe.scale,pe.height/2-r.y*pe.scale);
		pe.ellipse(0,0,20,20);

		pe.popMatrix();
		
		
	}
	
	public DataSet getLDataSet(int r, int g, int b) {
		return L_dataset.withColor(r,g,b);
	}

}
