package objects.pendulum;

import forces.Force;
import model.Main;
import objects.PEObject;
import processing.core.PVector;
import render.ForceThunk;
import render.PendRender;
import render.RenderThunk;
import render.StringThunk;

public class Pendulum1 implements PEObject, RenderThunk, PendRender{

	double theta;
	double omega;
	double L;
	
	double mass;
	
	Pendulum2 p2;
	
	//some fun graphing stuff :)
		PVector vLast=new PVector();
		PVector v=new PVector();
		PVector rLast=new PVector();
		PVector a=new PVector();
	
	PVector fulcrum;
	
	
	public Pendulum1(double mass, double L, PVector fulcrum) {
		this.L=L;
		this.mass=mass;
		this.fulcrum=fulcrum;
		Main.addRenderThunk(this);
		Main.addRenderThunk(new StringThunk(fulcrum, this));
//		Main.addRenderThunk(new ForceThunk(this,255,0,0) {
//			public PVector renderPos() {
//				return PVector.add(this.getFulcrum(), a);
//			}
//		});
//		Main.addRenderThunk(new ForceThunk(this,0,255,0) {
//			public PVector renderPos() {
//				return PVector.add(this.getFulcrum(), vLast);
//			}
//		});
	}
	
	public void setPendulum2(Pendulum2 pend2) {
		this.p2=pend2;
	}
	
	public void setTheta(double t) {
		theta=t;
	}
	
	@Override
	public void update(float dt) {
		//Euler because I lazy;
		omega+=omegaPrime()*dt;
		theta+=omega*dt;
		PVector pos = PVector.mult(PVector.fromAngle(getTheta()-3.14f/2f),getL());
		v = PVector.div(PVector.sub(pos,rLast),dt);
		//janky but it works??
		a = PVector.div(PVector.sub(v,vLast),dt);
		rLast=pos;
		vLast=v;
		
	}
	
	private double omegaPrime() {
		//scary line
		//see: https://www.myphysicslab.com/pendulum/double-pendulum-en.html
		double g=9.8;
		return ((-g * (2*mass + p2.getMass())*Math.sin(theta)) - p2.getMass()*g*Math.sin(theta-2*p2.getTheta()) - 2*Math.sin(theta-p2.getTheta())*p2.getMass()*(p2.getOmega()*p2.getOmega()*p2.getL() + omega*omega*L*Math.cos(theta-p2.getTheta()))) 
				/ (L * (2*mass+p2.getMass() - p2.getMass()*Math.cos(2*theta-2*p2.getTheta())));
	}
	
	public float getTheta() {
		return (float)theta;
	}
	
	public float getL() {
		return (float)L;
	}
	
	public double getOmega() {
		return omega;
	}

	@Override
	public void addForce(Force f) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public float getMass() {
		// TODO Auto-generated method stub
		return (float)mass;
	}

	@Override
	public PVector getVel() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public PVector renderPos() {
		return PVector.add(fulcrum, PVector.mult(PVector.fromAngle((float) (theta-3.14f/2f)),(float)L));

	}
	
	public void render(Main m) {
		PVector renderPos = renderPos();
		m.pushMatrix();
		m.translate(m.width/2+renderPos.x*m.scale,m.height/2-renderPos.y*m.scale);
		m.fill(0,255,0);
		m.ellipse(0,0,20,20);
		m.popMatrix();
		
	}

}
