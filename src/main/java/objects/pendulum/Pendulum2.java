package objects.pendulum;

import forces.Force;
import model.Main;
import objects.PEObject;
import processing.core.PVector;
import render.ForceThunk;
import render.PendRender;
import render.RenderThunk;
import render.StringThunk;

public class Pendulum2 implements PEObject, RenderThunk, PendRender{

	
	double theta;
	double omega;
	
	double mass;
	double L;
	
	//some fun graphing stuff :)
	PVector vLast=new PVector();
	PVector v=new PVector();
	PVector rLast=new PVector();
	PVector a=new PVector();
	
	Pendulum1 p1;
	
	
	public Pendulum2(double mass, double L, Pendulum1 p1) {
		this.p1=p1;
		this.L=L;
		this.mass=mass;
		Main.addRenderThunk(this);
		Main.addRenderThunk(new StringThunk(new PVector(), this) {
			public PVector getFulcrum() {
				return p1.renderPos();
			}
		});
		Main.addRenderThunk(new ForceThunk(this,255,0,0) {
			public PVector renderPos() {
				return PVector.add(this.getFulcrum(), PVector.sub(a,new PVector(0,-9.8f)));
			}
		});
		Main.addRenderThunk(new ForceThunk(this,0,255,0) {
			public PVector renderPos() {
				return PVector.add(this.getFulcrum(), vLast);
			}
		});
	}
	
	@Override
	public void update(float dt) {
		//Euler because I lazy;
		omega+=omegaPrime()*dt;
		theta+=omega*dt;
		PVector pos = PVector.add(p1.renderPos(), PVector.mult(PVector.fromAngle(getTheta()-3.14f/2f),getL()));
		v = PVector.div(PVector.sub(pos,rLast),dt);
		//janky but it works??
		a = PVector.div(PVector.sub(v,vLast),dt);
		rLast=pos;
		vLast=v;
		
	}
	
	public void setTheta(double t) {
		theta=t;
	}
	
	
	private double omegaPrime() {
		//scary line
		//see: https://www.myphysicslab.com/pendulum/double-pendulum-en.html
		double g=9.8;
		return (2*Math.sin(p1.getTheta()-theta)* (p1.getOmega()*p1.getOmega()*p1.getL()*(mass+p1.getMass()) + g*(mass+p1.getMass())*Math.cos(p1.getTheta()) + omega*omega*L*Math.cos(p1.getTheta()-theta) ) ) 
				/ (L* (2*p1.getMass() + mass - mass*Math.cos(2*p1.getTheta() - 2*theta)));
	}
	
	public float getL() {
		return (float) L;
	}
	
	public float getTheta() {
		return (float) theta;
	}
	
	public double getOmega() {
		return omega;
	}

	@Override
	public void addForce(Force f) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public float getMass() {
		// TODO Auto-generated method stub
		return (float)mass;
	}

	@Override
	public PVector getVel() {
		// TODO Auto-generated method stub
		return v;
	}
	
	public PVector getA() {
		return a;
	}

	public PVector renderPos() {
		return PVector.add(p1.renderPos(), PVector.mult(PVector.fromAngle(getTheta()-3.14f/2f),getL()));

	}
	
	@Override
	public void render(Main m) {
		PVector renderPos=renderPos();
		m.pushMatrix();
		m.translate(m.width/2+renderPos.x*m.scale,m.height/2-renderPos.y*m.scale);
		m.fill(0,255,0);
		m.ellipse(0,0,20,20);
		m.popMatrix();
		
		
	}
	

}
