package objects;

import model.Main;
import forces.Force;
import forces.Gravity;
import graph.DataSet;
import processing.core.PVector;
import render.ForceThunk;
import render.PendRender;
import render.RenderThunk;
import render.StringThunk;

public class PendulumObject extends RotationalObject implements PendRender{
	PVector v=new PVector();
	PVector rLast=new PVector();
	PVector vLast=new PVector();
	
	int r=0;
	int g = 255;
	int b = 0;

	public PendulumObject(PVector fulcrum, float itheta, float l, PVector iw, float m) {
		super(fulcrum, itheta, l, iw, m);
		Main.addRenderThunk(this);
		Main.addRenderThunk(new StringThunk(fulcrum,this));
		this.addForce(new Gravity(this));
		
		

	}
	
	public PendulumObject withRGB(int r, int g, int b) {
		this.r=r;
		this.g=g;
		this.b=b;
		return this;
	}
	
	public PendulumObject withForces() {
		Main.addRenderThunk(new ForceThunk(this,255,0,0) {
			public PVector renderPos() {
				return PVector.add(this.getFulcrum(), PVector.sub(a,new PVector(0,-9.8f)));
			}
		});
		Main.addRenderThunk(new ForceThunk(this,0,255,0) {
			public PVector renderPos() {
				return PVector.add(this.getFulcrum(), vLast);
			}
		});
		return this;
	}
	
	
	//constructor for physical pendulum
	public PendulumObject(PVector fulcrum, float itheta, float l, PVector iw, float m, float I) {
		super(fulcrum, itheta, l, iw, m, I);
		this.addForce(new Gravity(this));
		Main.addRenderThunk(new DiscPendulumThunk(this));

		Main.addRenderThunk(new StringThunk(fulcrum,this));

	}
	
	public PVector renderPos() {
		return PVector.add(fulcrum, PVector.mult(PVector.fromAngle(getTheta()-3.14f/2f),l));

	}

	@Override
	public void update(float dt) {
		super.update(dt);
		PVector pos = PVector.mult(PVector.fromAngle(getTheta()-3.14f/2f),getL());
		v = PVector.div(PVector.sub(pos,rLast),dt);
		//janky but it works??
		a = PVector.div(PVector.sub(v,vLast),dt);
		rLast=pos;
		vLast=v;
	}
	
	@Override
	public void render(Main m) {
		PVector renderPos = renderPos();
		m.pushMatrix();
		m.translate(m.width/2+renderPos.x*m.scale,m.height/2-renderPos.y*m.scale);
		m.fill(r,g,b);
		m.ellipse(0,0,20,20);
		m.popMatrix();
		
		
	}


	@Override
	public float getL() {
		// TODO Auto-generated method stub
		return l;
	}
	
}
