package objects.buoyant;

import forces.Buoyancy;
import forces.Gravity;
import model.Main;
import objects.TranslationalObject;
import processing.core.PConstants;
import processing.core.PVector;

public class BuoyantCube extends TranslationalObject implements BuoyancyObject{

	float radius;
	
	
	
	public BuoyantCube(PVector ir, float m, float radius) {
		super(ir, new PVector(), new PVector(), m);
		this.radius=radius;
		Main.addRenderThunk(this);
		addForce(new Buoyancy(this, 0));
		addForce(new Gravity(this));

	}

	@Override
	public void render(Main m) {
		m.pushMatrix();
		m.translate(m.width/2+r.x*m.scale,m.height/2-r.y*m.scale);
		m.rectMode(PConstants.CENTER);
		m.fill(0,255,0);
		m.rect(0,0,2f*radius*m.scale,2f*radius*m.scale);
		m.popMatrix();
		
	}

	@Override
	public float getVolumeSubmerged(float amtSubmerged) {
		// TODO Auto-generated method stub
		return 8f*radius*radius*amtSubmerged;
	}

	@Override
	public PVector getPosition() {
		// TODO Auto-generated method stub
		return r.copy();
	}

	@Override
	public float getRadius() {
		// TODO Auto-generated method stub
		return radius;
	}

}
