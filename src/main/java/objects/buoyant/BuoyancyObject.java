package objects.buoyant;

import objects.PEObject;
import processing.core.PVector;

public interface BuoyancyObject extends PEObject{

	public float getVolumeSubmerged(float amountSubmerged);
	public PVector getPosition();
	
	//ALEX TREBEK JUST DIED
	//also I hate this workaround but it works
	public float getRadius();
	
}
