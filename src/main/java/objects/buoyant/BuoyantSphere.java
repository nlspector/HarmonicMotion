package objects.buoyant;

import forces.Buoyancy;
import forces.Gravity;
import model.Main;
import objects.TranslationalObject;
import processing.core.PConstants;
import processing.core.PVector;

public class BuoyantSphere extends TranslationalObject implements BuoyancyObject{

	float radius;
	
	public BuoyantSphere(PVector ir, float m, float radius) {
		super(ir, new PVector(), new PVector(), m);
		this.radius=radius;
		Main.addRenderThunk(this);
		addForce(new Buoyancy(this, 0));
		addForce(new Gravity(this));

	}

	@Override
	public void render(Main m) {
		m.pushMatrix();
		m.translate(m.width/2+r.x*m.scale,m.height/2-r.y*m.scale);
		m.ellipseMode(PConstants.CENTER);
		m.fill(255,0,0);
		m.ellipse(0,0,2f*radius*m.scale,2f*radius*m.scale);
		m.popMatrix();
		
	}

	@Override
	public float getVolumeSubmerged(float amtSubmerged) {
		// TODO Auto-generated method stub
		//i silly boy so i subtract radius because can;
		float pr = amtSubmerged-radius;
		return (float)(3.14159*( radius*radius*pr- 0.33333*Math.pow(pr,3) + 0.66667*Math.pow(radius,3)));
	}

	@Override
	public PVector getPosition() {
		// TODO Auto-generated method stub
		return r.copy();
	}

	@Override
	public float getRadius() {
		// TODO Auto-generated method stub
		return radius;
	}
}