package objects.buoyant;

import model.Main;
import processing.core.PConstants;
import render.RenderThunk;

public class WaterThunk implements RenderThunk{

	float height;

	public WaterThunk(float height) {
		this.height=height;
	}
	
	@Override
	public void render(Main m) {
		m.pushMatrix();
		m.pushStyle();
		m.translate(0,m.height/2-this.height*m.scale);
		m.fill(0,0,255,100);
		m.rectMode(PConstants.CORNER);
		m.rect(0,0,m.width,m.height);
		m.popMatrix();
		m.popStyle();
	}
	
	

}
