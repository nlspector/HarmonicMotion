package objects;

import java.util.ArrayList;
import java.util.List;

import forces.Force;
import graph.DataSet;
import model.Listener;
import model.Main;
import model.Observation;
import processing.core.PVector;
import render.PendRender;
import render.RenderThunk;

public abstract class RotationalObject implements RenderThunk, PEObject {

	private List<Force> forces;
	protected float theta;
	protected PVector w;
	protected PVector a;
	protected PVector fulcrum;

	protected float I;
	protected float m;
	protected float l;
	
	double t;
	
	DataSet t_dataset=new DataSet(255,0,0,"time","angle");
	DataSet w_dataset=new DataSet(255,0,0,"time","angular speed");
	DataSet a_dataset=new DataSet(255,0,0,"time","angular acceleration");

	
	public RotationalObject(PVector fulcrum, float itheta, float l, PVector iw, float m) {
		theta=itheta;
		w=iw;
		a=new PVector();
		this.m=m;
		this.I=m*l*l;
		this.l=l;
		this.fulcrum=fulcrum;
		forces = new ArrayList<>();
		
	}
	
	public RotationalObject(PVector fulcrum, float itheta, float l, PVector iw, float m, float I) {
		theta=itheta;
		w=iw;
		a=new PVector();
		this.I=I;
		this.m=m;
		this.l=l;
		this.fulcrum=fulcrum;
		forces = new ArrayList<>();
		
	}
	
	
	
	public float getMass() {
		return m;
	}
	
	public void addForce(Force force) {
		forces.add(force);
	}

	public void update(float dt) {
		t+=dt;
		PVector sumTorques= new PVector();
		//theta=0 is when the pendulum is straight down
		//theta=pi/2 is pendulum far right
		//normal coordinate system yay
		PVector pos = PVector.mult(PVector.fromAngle(getTheta()-3.14f/2f),l);
		for(int i = 0; i < forces.size(); i++) {
			sumTorques.add(pos.cross(forces.get(i).getForce(dt)));
		}
		a = PVector.div(sumTorques, I);
		w.add(PVector.mult(a, dt));
		theta = getTheta() + w.z*dt;
		a_dataset.addPoint(t,a.mag());
		w_dataset.addPoint(t,w.mag());

		t_dataset.addPoint(t,getTheta());

		
		if(Math.abs(w.z) <0.05) Main.printTime();
	}
	
	public PVector getVel() {
		return null;
	}
	
	List<Listener> listeners = new ArrayList<>();
	
	public void addListener(Listener l) {
		listeners.add(l);
	}
	
	public void removeListener(Listener l) {
		listeners.remove(l);
	}
	
	public void observation(Observation o) {
		for(Listener l: listeners) {
			l.observed(o);
		}
	}
	
	public DataSet getThetaDataSet(int r, int g, int b) {
		return t_dataset.withColor(r,g,b);
	}

	public float getTheta() {
		return theta;
	}
	

}
