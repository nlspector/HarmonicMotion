package objects;

import forces.Force;
import processing.core.PVector;

public interface PEObject {

	
	public void update(float dt);
	public void addForce(Force f);
	public float getMass();
	
	public PVector getVel();
}
