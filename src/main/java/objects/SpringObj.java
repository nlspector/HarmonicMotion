package objects;

import forces.SpringForce;
import model.Main;
import processing.core.PVector;
import render.RenderThunk;

public class SpringObj extends TranslationalObject{

	public SpringObj(PVector ir, PVector iv, PVector ia, float m) {
		super(ir, iv, ia, m);
		Main.addRenderThunk(this);
		addForce(new SpringForce(this, new PVector(),0));

	}
	
	
	
	
	public SpringObj(PVector ir, PVector iv, PVector ia, float m, boolean b) {
		super(ir, iv, ia, m);
		Main.addRenderThunk(this);
		//addForce(new SpringForce(this, new PVector()));
		
	}

	@Override
	public void render(Main pe) {
		
		pe.pushMatrix();
		pe.translate(pe.width/2+r.x*pe.scale,pe.height/2-r.y*pe.scale);
		pe.ellipse(0,0,pe.scale/2.0f,pe.scale/2.0f);
		pe.popMatrix();
		
	}

}
