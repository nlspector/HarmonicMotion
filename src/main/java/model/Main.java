package model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import forces.Driver;
import forces.KVFriction;
import forces.SpringForce;
import graph.GraphWindow;
import objects.TranslationalObject;
import objects.buoyant.BuoyantCube;
import objects.buoyant.BuoyantSphere;
import objects.DiscPendulumThunk;
import objects.PEObject;
import objects.PendulumObject;
import objects.RotationalObject;
import objects.SpringObj;
import objects.buoyant.WaterThunk;
import objects.pendulum.Pendulum1;
import objects.pendulum.Pendulum2;
import objects.pendulum.TPendulumObject;
import processing.core.PApplet;
import processing.core.PVector;
import render.RenderThunk;
import render.ScaleRender;

public class Main extends PApplet{

	static float t=0;
	private static float dt=0.0016667f;
	public float scale=200f;

	static List<PEObject> objects =new ArrayList<>();
	static List<RenderThunk> toRender = new ArrayList<>();
	GraphWindow gw;
		
	LinkedList<Simulation> sim;
	
	public static void main(String[] args) {
		PApplet.main(new String[] {Main.class.getName()});
		
		
	}
	
	@Override
	public void settings() {
		size(960,640);
		gw = new GraphWindow();

	}
		
	@Override
	public void setup() {
		frameRate(1/dt);
		addRenderThunk(new ScaleRender());
		
		
		//pendulum error
//		PendulumObject o= new PendulumObject(new PVector(0,1), 3.14f/2f, 1, new PVector(), 100f).withRGB(255,255,255);
//		addObject(o);
//		TPendulumObject tpo = new TPendulumObject(new PVector(1f,1f), 1, new PVector(0,1f));
//		TPendulumObject tpo2 = new TPendulumObject(new PVector(1f,1f), 1, new PVector(0,1));
//		tpo2.setRk(true);
//
//		addObject(tpo);
//		addRenderThunk(tpo);
//		addObject(tpo2);
//		addRenderThunk(tpo2);
//
//		gw.addDataSet(tpo.getLDataSet(255,0,0));
//		gw.addDataSet(tpo2.getLDataSet(0,255,0));
		
		
		//first demos
//		sim = new LinkedList<>();
//		sim.add(new SpringSim());
//		sim.add(new PendSim());
//		sim.add(new PhysPendSim());
//		sim.add(new WaterSim());
//		sim.remove().run();
		
		
//		//two springs
//		SpringObj s = new SpringObj(new PVector(1f,0,0), new PVector(), new PVector(),1,false);
//		s.addForce(new KVFriction(1,s));
//
//		s.addForce(new SpringForce(s,new PVector(),1));
//		SpringObj s2 = new SpringObj(new PVector(3,0,0), new PVector(), new PVector(), 1, false);
//		SpringForce sf = new SpringForce(s,s2,1f).withK(16).withRGB(0,0,255); 
//		s.addForce(sf);
//		s2.addForce(sf.invert());
//		s2.addForce(new KVFriction(1,s2));
//		addObject(s);
//		addObject(s2);
//		s.addForce(new Driver(15).withPeriod(2));
//		gw.addDataSet(s.getXPositionDataSet(255,0,0));
//		gw.addDataSet(s2.getXPositionDataSet(0,0,255));
		
		
//		//springs on y axis
//		SpringObj s = new SpringObj(new PVector(1f,.5f,0), new PVector(), new PVector(),100,false);
//
//		s.addForce(new SpringForce(s,new PVector(),1).withK(1000).withRGB(0,255,0));
//		s.addForce(new SpringForce(s,new PVector(2,0,0),1).withK(1000));
//		addObject(s);
		
//		scale=30f;
//		lattice(10,10);
		
		//damped and driven demos
//		sim = new LinkedList<>();
//		sim.add(new SpringSim());
//
//		sim.add(new DampedDrivenSim(0,2));
//		sim.add(new DampedDrivenSim(0,1.57));
//		
//		sim.add(new DampedDrivenSim(16,0));
//		sim.add(new DampedDrivenSim(1,0));
//		
//		sim.add(new DampedDrivenSim(1,2));
//
//		sim.remove().run();

		

//		//double pendulum :)
		scale=150f;
		Pendulum1 p1 = new Pendulum1(2,1, new PVector());
		Pendulum2 p2 = new Pendulum2(2,1, p1);
		p1.setPendulum2(p2);
		p1.setTheta(Math.PI/3);
		p2.setTheta(Math.PI/2);
		addObject(p1);
		addObject(p2);
//
		addRenderThunk(m -> {
			m.textSize(24);
			m.fill(255);
			m.text("t: "+t,0,height);
		});
	
	}
	
	
	public void lattice(int n, int m) {
		double ck = 100;
		SpringObj[][] arr = new SpringObj[n][m];
		for(int x = 0; x < n; x++) {
			for(int y = 0; y < m; y++) {
				SpringObj obj = new SpringObj(new PVector(x,y,0), new PVector(), new PVector(),1,false);

				if(x==0)obj.addForce(new SpringForce(obj,new PVector(-1,y,0),0.9).withK(ck).withRGB(0,255,0));
				else connect(obj, arr[x-1][y],ck,255,0,0);
				if(x==n-1)obj.addForce(new SpringForce(obj,new PVector(n,y,0),0.9).withK(ck).withRGB(0,255,0));
				
				if(y==0)obj.addForce(new SpringForce(obj,new PVector(x,-1,0),0.9).withK(ck).withRGB(0,255,0));
				else connect(obj, arr[x][y-1],ck,255,0,0);
				if(y==m-1)obj.addForce(new SpringForce(obj,new PVector(x,m,0),0.9).withK(ck).withRGB(0,255,0));
				
				arr[x][y]=obj;
				obj.addForce(new KVFriction(0.4f,obj));

				addObject(obj);
			}
		}
		arr[3][3].addForce(new Driver(6).withPeriod(.157));

		//arr[3][3].setPos(new PVector(3.5f,3.5f));
	}
	
	
	public void connect(SpringObj o1, SpringObj o2, double k, int r, int g, int b) {
		SpringForce sf = new SpringForce(o1,o2,0.9f).withK(k).withRGB(r,g,b); 
		o1.addForce(sf);
		o2.addForce(sf.invert());
	}
	
	public static void printTime() {
		System.out.println("time: " + t);
	}
	
	public static void printTime2() {
		System.out.print("time: " + t);
	}
	
	@Override
	public void draw() {	
		if(!mousePressed) {
			t+=dt;
			//first update objects
			for(int i = 0; i<objects.size(); i++) {
				objects.get(i).update(dt);
			}
		}
		background(0);
		//then execute RenderThunks
		for(int i = 0; i<toRender.size(); i++) {
			toRender.get(i).render(this);
		}
		
	}
	
	public static void addObject(PEObject pe) {
		//addRenderThunk(pe);
		objects.add(pe);
	}
	
	public static void addRenderThunk(RenderThunk r) {
		toRender.add(r);
	}
	
	public void keyPressed() {
		if(key==' ') {
			if(sim!=null) {
				if(sim.size() !=0) {
					SpringForce.numForces=0;
					sim.remove().run();
				}

			}
		}
	}
	
	
	
	//code to run "slideshow" effect
	
	public class Simulation {
		public void run() {
			objects =new ArrayList<>();
			toRender = new ArrayList<>();
			gw.removeAllSets();
			gw.resetPoints();
			t=0;
			addRenderThunk(m -> {
				m.textSize(24);
				m.fill(255);
				m.text("t: "+t,0,height);
			});
			addRenderThunk(new ScaleRender());

		}
	}
	
	public class SpringSim extends Simulation {
		@Override
		public void run() {
			super.run();
			SpringObj s = new SpringObj(new PVector(1,0,0), new PVector(), new PVector(), 1);
			addObject(s);
			gw.addDataSet(s.getXPositionDataSet(255,0,0));
			
		}
		
	}

	public class PendSim extends Simulation {
		@Override
		public void run() {
			super.run();
			//simple pendulum (rotationally based)
			PendulumObject o= new PendulumObject(new PVector(0,0), 3.14f/2f, 1, new PVector(), 100f);
			addObject(o);
			gw.addDataSet(o.getThetaDataSet(0,255,0));
			
		}
		
	}
	public class PhysPendSim extends Simulation {
		@Override
		public void run() {
			super.run();
			//physical pendulum (rotationally based)
			//fulcrum, itheta, length, iw, mass
			//using RADIUS of disc = 1
			scale=100f;
			PendulumObject physicalPend1 = new PendulumObject(new PVector(0,0), 3.14f/12f, 1, new PVector(), 100f, 
					0.5f*100f*1f*1f+100f*1f*1f); /*moment of inertia = Icm + Ml^2 = 1/2Ml^2*/
			addObject(physicalPend1);
			gw.addDataSet(physicalPend1.getThetaDataSet(255,0,0));
			
		}
		
	}
	public class WaterSim extends Simulation {
		@Override
		public void run() {
			super.run();
			//water
			BuoyantCube c = new BuoyantCube(new PVector(0,-0.1f),1000,0.5f);
			BuoyantSphere s = new BuoyantSphere(new PVector(1,-0.1f),500,0.5f);
		
			addObject(c);
			addObject(s);
			gw.addDataSet(c.getYPositionDataSet(0,255,0));
			gw.addDataSet(s.getYPositionDataSet(255,0,0));
			addRenderThunk(new WaterThunk(0f));
			addRenderThunk(m -> {
				m.textSize(24);
				m.fill(255);
				m.text("t: "+t,0,height);
			});
			addRenderThunk(new ScaleRender());

		}
		
	}
	
	public class DampedDrivenSim extends Simulation{
		float b;
		double T;
		
		public DampedDrivenSim(float b, double T) {
			this.b=b;
			this.T=T;
		}

		@Override
		public void run() {
			super.run();
			SpringObj s = new SpringObj(new PVector(1,0,0), new PVector(), new PVector(), 1);
			addObject(s);
			
			//in phase = 1.57
			if(T!=0) s.addForce(new Driver(15).withPeriod(T));
			s.addForce(new KVFriction(b,s));
			gw.addDataSet(s.getXPositionDataSet(255,0,0));
		}
		
	}
}

