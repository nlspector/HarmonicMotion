package model;
import java.util.ArrayList;
import java.util.List;

public class Observer {
	
	List<Listener> listeners = new ArrayList<>();
	
	public void addListener(Listener l) {
		listeners.add(l);
	}
	
	public void removeListener(Listener l) {
		listeners.remove(l);
	}
	
	public void observation(Observation o) {
		for(Listener l: listeners) {
			l.observed(o);
		}
	}

}
