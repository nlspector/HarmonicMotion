package graph;

public class Point{
	public double x;
	public double y;
	
	public int r,g,b;
	
	public Point(double x, double y) {
		this.x=x;
		this.y=y;
	}
	
	public void setColor(int r, int g, int b) {
		this.r=r;
		this.g=g;
		this.b=b;
	}
	
	public void render(GraphWindow gw) {
		gw.stroke(r,g,b);
		gw.strokeWeight(2);
		float gx = (float) (x*360/gw.xMax+60);
		float gy = (float) (180f-180/gw.yMax*y);
		gw.ellipse(gx,gy,1,1);
	}

}
