package graph;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import model.Listener;
import model.Observation;

public class DataSet {
	
	public int r;
	public int g;
	public int b;
	
	public String xLabel;
	public String yLabel;
	
	boolean visible=true;
		
	List<Point> points=new ArrayList<>();
	
	public DataSet (int r, int g, int b, String xl, String yl) {
		this.r=r;
		this.g=g;
		this.b=b;
		this.xLabel=xl;
		this.yLabel=yl;
	}
	
	public DataSet withColor(int r, int g, int b) {
		this.r=r;
		this.g=g;
		this.b=b;
		return this;
	}
	
	public void addPoint(Point p) {
		p.setColor(r,g,b);
		points.add(p);
		observation(p);
	}
	
	public void addPoint(double x, double y) {
		addPoint(new Point(x,y));
	}

	public void setVisible(boolean b) {
		visible=b;
	}
	
	public boolean isVisible() {
		return visible;
	}
	
	//Observer implementation
	//only graphwindows will be listening in
	List<GraphWindow> listeners = new ArrayList<>();
	
	public void addListener(GraphWindow l) {
		listeners.add(l);
	}
	
	public void removeListener(GraphWindow l) {
		listeners.remove(l);
	}
	
	public void observation(Point p) {
		for(GraphWindow l: listeners) {
			l.graphNextPoint(p);
		}
	}

	public List<Point> getPoints(){
		return points;
	}
	
	
}
