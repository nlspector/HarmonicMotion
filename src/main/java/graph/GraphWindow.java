package graph;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import model.Main;
import objects.PEObject;
import processing.core.PApplet;

//PURELY GRAPHICAL CLASS
public class GraphWindow extends PApplet{
	
	//what's being tracked
	PEObject target;
	
	//a log of the points to graph	
	private Stack<Point> toGraph = new Stack<>();
	
	private List<DataSet> sets=new ArrayList<>();
	
	//maximum measurements that need to be displayed
	float xMax = 1f;
	float yMax = 0.1f;
	
	//axis labels
	//private String xLabel = "";
	//private String yLabel = "";
		
	private boolean doReset = false;
		
	public GraphWindow() {
		PApplet.runSketch(new String[]{this.getClass().getSimpleName()}, this);
		//xLabel = x;
		//yLabel = y;
	}
	
	
	@Override
	public void settings() {
		size(480,360);
	}
	
	@Override
	public void setup() {
		reset();
		
	}
	
	@Override
	public void keyPressed() {

	}
	
	public void resetBounds() {
		yMax = 0.1f;
		xMax=1;
	}
	
	public void addDataSet(DataSet set) {
		set.addListener(this);
		sets.add(set);
		redraw();
	}
	
	public void removeDataSet(DataSet set) {
		set.removeListener(this);
		sets.remove(set);
		redraw();
	}
	
	public void removeAllSets() {
		while(sets.size() > 0) {
			sets.remove(0).removeListener(this);
		}
		resetBounds();
	}
	
	//hard reset--for a new graph
	//resets all variables
	public void reset() {
		resetGraph();
		doReset=false;
	}
	
	
	//this function is called whenever there is new data
	public void graphNextPoint(Point p) {
		toGraph.add(p);
	}
	
	public void resetPoints() {
		//tell the draw method to reset on the next go-around
		doReset=true;
		toGraph=new Stack<>();
	}
	
	
	@Override
	public void draw() {
		//first let's see if a reset was requested
		//and if so reset everything
		if(doReset) {
			reset();
			return;
		}
		//if there is new data
		while(toGraph.size()!=0) {
			//get the new point we added
			Point p = toGraph.pop();
			//adjust scaling and see if we need to scale up
			boolean redraw = false;
			while(xMax < Math.abs(p.x)) {
				xMax*=2;
				redraw=true;
			}
			while(yMax < Math.abs(p.y)) {
				yMax*=2;
				redraw=true;
			}
			if(redraw) {
				//redraw will update all the points
				redraw();
			} else {
				p.render(this);
			}
		}
	}
	
	//set what we're tracking
	public void setTarget(PEObject obj) {
		target = obj;
	}
	
	//since we are not drawing the background every time
	//this function will redraw the background and axes on top of the data
	//does not reset variables, just display
	public void resetGraph() {
		background(255);
		drawGrid();
		stroke(0);
		strokeWeight(2);
		line(60,0,60,360);
		line(0,180,480,180);
		fill(0);
		//textSize(24);
		//text(yLabel, 72,30);
		//text(xLabel, 480-xLabel.length()*15, 162);
		fill(255,0,0);
		
	}
	
	//this method draws a light gray grid behind
	//with measurements
	public void drawGrid() {
		stroke(180);
		strokeWeight(1);
		fill(140);
		textSize(10);
		//draw one y-line every 40 pixels
		//and scale accordingly
		for(int yInt = 20; yInt<=340;yInt+=40) {
			line(0,yInt,480,yInt);
			double val = Math.floor(yMax/180*(180-yInt)*100)/100;
			if(Math.abs(val) <1000000000) text(val + "", 60, yInt);
		}
		//same with xs
		for(int xInt = 20; xInt<=460;xInt+=40) {
			line(xInt,0,xInt,360);
			double val = Math.floor(xMax/420*(xInt-60)*100)/100;
			text(val + "", xInt,180);
		}
	}
	
	
	//this function goes through each point and splats it on the canvas
	//used whenever we need to scale up or remove a dataset
	public void redraw() {
		resetGraph();
		for(DataSet d: sets) {
			for(int i =0; i<d.getPoints().size(); i++) d.getPoints().get(i).render(this);
		}
		
	}

}
