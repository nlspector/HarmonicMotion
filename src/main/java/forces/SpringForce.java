package forces;

import model.Listener;
import model.Main;
import model.Observation;
import objects.TranslationalObject;
import processing.core.PVector;
import render.RenderThunk;
import render.SpringRender;

public class SpringForce implements Listener, Force, RenderThunk{

	double k = 16;
	TranslationalObject obj;
	
	
	TranslationalObject obj2;

	PVector origin;
	double eq;
		
	public static int numForces = 0;
	int forceInd = 0;
	
	PVector spring;
	
	SpringRender s;
	
	@Override
	public void observed(Observation o) {
		k=o.getK();
	}
	
	public SpringForce(TranslationalObject obj, PVector origin) {
		this(obj,origin,0);

	}
	
	public SpringForce(TranslationalObject obj, PVector origin, double eq) {
		this(obj,origin,eq,true);
	}
	
	public SpringForce(TranslationalObject obj, PVector origin, double eq, boolean rend) {
		this.obj=obj;
		this.origin=origin;
		this.eq=eq;
		PVector spring =PVector.sub(obj.getPos(), origin);
		this.spring=spring;

		forceInd=numForces++;
		if(rend) {
			Main.addRenderThunk(this);
			Main.addRenderThunk(s=new SpringRender(origin, obj));
		}


	}
	
	public SpringForce(TranslationalObject obj, TranslationalObject o2) {
		this(obj,o2,PVector.sub(obj.getPos(), o2.getPos()).mag());
	}
	
	public SpringForce(TranslationalObject obj, TranslationalObject o2, double eq, boolean rend) {
		this.obj=obj;
		this.obj2=o2;
		this.eq=eq;
		
		forceInd=numForces++;
		PVector spring =PVector.sub(obj.getPos(), obj2.getPos());
		this.spring=spring;

		if(rend) {
			Main.addRenderThunk(this);
			Main.addRenderThunk(s=new SpringRender(o2, obj));
		}


	}
	
	public SpringForce(TranslationalObject obj, TranslationalObject o2, double eq) {
		this(obj,o2,eq,true);


	}
	
	public SpringForce withRGB(int r, int g, int b) {
		s.setRGB(r,g,b);
		return this;
	}
	
	public SpringForce invert() {
		return new SpringForce(obj2,obj,eq,false).withK(k);
	}
	
	public float sig(PVector in) {
		return Math.signum(in.dot(spring));
//		if(sig_x==0){
//			return (sig_y == Math.signum(spring.y)) ? 1 : -1;
//		} else if(sig_y==0){
//			return (sig_x == Math.signum(spring.x)) ? 1 : -1;
//		}
//		else {
//			return (sig_x == Math.signum(spring.x) || sig_y == Math.signum(spring.y)) ? 1 : -1;
//		}

	}
	
	public SpringForce withK(double k) {
		this.k=k;
		return this;
	}


	@Override
	public PVector getForce(float dt) {
		PVector spring = new PVector();
		if(obj2==null) {
			spring =PVector.sub(obj.getPos(), origin);
		}
		else { 
			spring =PVector.sub(obj.getPos(), obj2.getPos());
		}
		//spring.mag()
		//System.out.println(PVector.sub(spring, PVector.mult(spring.copy().normalize(), (float) eq)));
		return PVector.mult( PVector.sub(spring, PVector.mult(spring.copy().normalize(), (float)eq*sig(spring))), (float) -k);


	}

	@Override
	public void render(Main m) {
		m.textSize(24);
		m.text("k: "+k,0,24+48*forceInd);
		if(obj2==null) m.text("x: "+PVector.sub(obj.getPos(), origin).mag(),0,48+48*forceInd);
		else m.text("x: "+PVector.sub(obj.getPos(), obj2.getPos()).mag(),0,48+48*forceInd);
		

		
	}
	

}
