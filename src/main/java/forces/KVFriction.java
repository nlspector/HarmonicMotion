package forces;

import objects.PEObject;
import processing.core.PVector;

public class KVFriction implements Force{
	
	float k;
	PEObject o;
	
	public KVFriction(float k, PEObject o) {
		this.k=k;
		this.o=o;
	}
	
	public PVector getForce(float dt) {
		return PVector.mult(o.getVel(),-k);
	}
}
