package forces;

import processing.core.PVector;

public interface Force {
	
	
	public PVector getForce(float dt);
	
	public default PVector getForce(float dt, PVector r, PVector v) {
		return getForce(dt);
	}
	
}
