package forces;

import objects.buoyant.BuoyancyObject;
import processing.core.PVector;

public class Buoyancy implements Force{
	
	float rho=1000f; //in kg/m^3
	float g=9.8f; // m/s^2
	BuoyancyObject o;
	
	float waterLevel;
	
	public Buoyancy(BuoyancyObject o, float wl) {
		this.o=o;
		waterLevel=wl;
	}

	@Override
	public PVector getForce(float dt) {
		return new PVector(0,rho*g*o.getVolumeSubmerged(amountSubmerged()));
	}
	
	private float amountSubmerged() {
		float raw = o.getRadius()-(o.getPosition().y-waterLevel);
		return Math.min(Math.max(0,raw),o.getRadius()*2f);
	}
	

}
