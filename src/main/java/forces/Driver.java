package forces;

import processing.core.PVector;

public class Driver implements Force {

	double amplitude;
	double angularFrequency;
	PVector dir=new PVector(1,0);
	
	double t;
	
	public Driver(double amp) {
		amplitude=amp;
	}

	public Driver withAngularFrequency(double angularFreq) {
		setAngularFrequency(angularFreq);
		return this;
	}
	
	public Driver withPeriod(double sec) {
		setPeriod(sec);
		return this;
	}
	
	public Driver withDirection(PVector dir) {
		setDirection(dir);
		return this;
	}
	
	public void setAngularFrequency(double angularFreq) {
		angularFrequency=angularFreq;
	}
	
	public void setPeriod(double sec) {
		angularFrequency=2*Math.PI/sec;
	}
	
	public Driver setDirection(PVector dir) {
		this.dir=dir.copy().normalize();
		return this;
	}
	
	@Override
	public PVector getForce(float dt) {
		t+=dt;
		double coeff=amplitude * Math.cos(angularFrequency*t);
		return PVector.mult(dir, (float)coeff);
	}

}
