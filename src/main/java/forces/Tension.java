package forces;

import objects.TranslationalObject;
import processing.core.PVector;

public class Tension implements Force{
	
	TranslationalObject o;
	PVector fulcrum;
	Gravity g;
	float L;
	
	public Tension(TranslationalObject o, PVector fulcrum, Gravity g, float L) {
		this.o=o;
		this.fulcrum=fulcrum;
		this.g=g;
		this.L=L;
	}

	@Override
	public PVector getForce(float dt) {
		PVector rod = PVector.sub(fulcrum, o.getPos());
		L=rod.mag();
		rod.normalize();
		float mgComp = PVector.dot(rod, g.getForce(dt));
		float centripetal = o.getMass()* o.getVel().magSq()/L;
		return PVector.mult(rod, centripetal-mgComp);
	}
	
	@Override
	public PVector getForce(float dt, PVector r, PVector v) {
		PVector rod = PVector.sub(fulcrum, r);
		L=rod.mag();
		rod.normalize();
		float mgComp = PVector.dot(rod, g.getForce(dt));
		float centripetal = o.getMass()* v.magSq()/L;
		return PVector.mult(rod, centripetal-mgComp);
	}

}
