package forces;

import objects.PEObject;
import processing.core.PVector;

public class Gravity implements Force{
	
	PEObject o;
	
	public Gravity(PEObject o) {
		this.o=o;
	}

	@Override
	public PVector getForce(float dt) {
		return new PVector(0,-9.8f*o.getMass());
	}

}
